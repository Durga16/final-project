class Unlogged < ApplicationMailer

  def sendUnloggedmail(mail,fname,lname)
    @umail=mail
    @firstname=fname
    @lastname=lname
    @current_date=Date.today.to_s
    mail(to:@umail,
    from:"services@gmail.com",
    subject:"Redmine Unlogged "+@current_date)
  end
end
