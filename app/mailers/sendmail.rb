class Sendmail < ApplicationMailer

  def mailing(adminemailid,user,holidayUser,unlogged)
    @users=user
    @current_date=Date.today.to_s
    @holidayUsers=holidayUser
    @unloggedusers=unlogged
    mail(to:adminemailid,
    from:"services@gmail.com",
    subject:"Redmine Logged Users "+@current_date)
  end

end
