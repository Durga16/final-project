# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $('#mailalert_adminlist').autocomplete
  source: $('#mailalert_adminlist').data('autocomplete-source')

  jQuery ->
    $('#mailalert_excluList').autocomplete
    source: $('#mailalert_excluList').data('autocomplete-source')
