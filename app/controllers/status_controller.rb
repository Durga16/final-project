class StatusController < ApplicationController
  before_action :tracker, only: [:waiting, :verification, :closed, :stagnated]
  def dashboard
    begin
      @db = Hash.new()
      case request.method_symbol
      when :get
        @user = User.find(:all)
        us = @user.to_json
        @un = JSON.parse(us)
        RedmineUser.delete_all
        @un.each do |ss|

          #to store the users in database
          ruser=RedmineUser.create(name: ss['login'],mailid: ss['mail'], firstname: ss['firstname'], lastname: ss['lastname'])
          @redmineUsers=RedmineUser.all
        end
        @in = Array.new()
        i=0
        @status = [10,2,8,11,7,4]
        @un.each do |item|
          j=0
          @status.each do |stat|
            @index= Issue.find(:all, :params => { :assigned_to_id => item['id'], :status_id => stat })
            disp = @index.to_json
            @in[j] = JSON.parse(disp)
            j+=1
          end
          @db[i]={"Assignee"=>item['firstname'] + " " + item['lastname'],"rep"=>@in[0].count,"ipp"=>@in[1].count,"vep"=>@in[2].count,"dop"=>@in[3].count,"wap"=>@in[4].count,"fep"=>@in[5].count}
          i+=1
        end

      when :post

        from = params[:updatef]
        to = params[:updatet]
        @us = RedmineUser.all
        i=0
        @us.each do |item|
          data = Changedissue.where({Assignee_Name: item.firstname + " " + item.lastname ,Updated_Time: Date.strptime(from,"%m/%d/%Y")..Date.strptime(to,"%m/%d/%Y")})
          @in =[0,0,0,0,0,0]
          @out=[0,0,0,0,0,0]
          if data.length!=0
            data.each do |dat|
              if dat.New_Status_Name=="Ready"
                @in[0]+=1
              elsif dat.New_Status_Name=="In Progress"
                @in[1]+=1
              elsif dat.New_Status_Name=="Verification"
                @in[2]+=1
              elsif dat.New_Status_Name=="Done"
                @in[3]+=1
              elsif dat.New_Status_Name=="Waiting"
                @in[4]+=1
              elsif dat.New_Status_Name=="Feedback"
                @in[5]+=1
              end
              if dat.Old_Status_Name=="Ready"
                @out[0]+=1
              elsif dat.Old_Status_Name=="In Progress"
                @out[1]+=1
              elsif dat.Old_Status_Name=="Verification"
                @out[2]+=1
              elsif dat.Old_Status_Name=="Done"
                @out[3]+=1
              elsif dat.Old_Status_Name=="Waiting"
                @out[4]+=1
              elsif dat.Old_Status_Name=="Feedback"
                @out[5]+=1
              end
            end
            (0..6).each do |k|
              if @in[k]==0 && @out[k]==0
                @in[k]="-"
                @out[k]=" "
              elsif @in[k]==0
                @in[k]=" "
                @out[k]= "-"+@out[k].to_s
              elsif @out[k]==0
                @out[k]=" "
                @in[k]=  "+"+@in[k].to_s
              else
                @in[k]= "+"+ @in[k].to_s
                @out[k]= "-"+@out[k].to_s
              end
            end
            @db[i]={"Assignee"=>data[0].Assignee_Name,"rep"=>@in[0].to_s+" "+@out[0].to_s,"ipp"=>@in[1].to_s+"  "+@out[1].to_s,"vep"=> @in[2].to_s+"  "+@out[2].to_s,"dop"=> @in[3].to_s+"  "+@out[3].to_s,"wap"=> @in[4].to_s+"  "+@out[4].to_s,"fep"=> @in[5].to_s+"  "+@out[5].to_s}
            i+=1
          end
        end
      end
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def waiting
    begin
      case request.method_symbol
      when :get
        @waiting= Issue.find(:all, :params => { :status_id => "7" })
      when :post

        @trac = params[:tracker]
        if (@trac == "0")
          @waiting= Issue.find(:all, :params => { :status_id => "7" })
        else
          @waiting= Issue.find(:all, :params => { :status_id => "7", :tracker_id => @trac })
        end
      end
      disp = @waiting.to_json
      @d1 = JSON.parse(disp)
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def verification
    begin
      case request.method_symbol
      when :get
        @issue= Issue.find(:all, :params => { :status_id => "8" })
      when :post
        @trac = params[:tracker]
        if (@trac == "0")
          @issue= Issue.find(:all, :params => { :status_id => "8" })
        else
          @issue= Issue.find(:all, :params => { :status_id => "8", :tracker_id => @trac })
        end
      end
      disp = @issue.to_json
      @d = JSON.parse(disp)
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def closed
    begin
      case request.method_symbol
      when :get
        @closed= Issue.find(:all, :params => { :status_id => "5" })
      when :post
        @trac = params[:tracker]
        if (@trac == "0")
          @closed= Issue.find(:all, :params => { :status_id => "5" })
        else
          @closed= Issue.find(:all, :params => { :status_id => "5", :tracker_id => @trac })
        end
      end
      disp = @closed.to_json
      @d2 = JSON.parse(disp)
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def stagnated
    begin
      @days = Toolsdb.find(4).password
      case request.method_symbol
      when :get
        @stagnated= Issue.find(:all, :params => { :limit => 100 })
        puts @stagnated.length
      when :post
        @trac = params[:tracker]
        if (@trac == "0")
          @stagnated= Issue.find(:all, :params => { :limit => 100 })
        else
          @stagnated= Issue.find(:all, :params => { :tracker_id => @trac })
        end
      end
      disp = @stagnated.to_json
      @d3 = JSON.parse(disp)
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def organization
    begin
      case request.method_symbol
      when :get
        puts Checkdb.where(date:Date.today).length

      when :post
        @po=Array(params[:organization])
        if(@po!=nil)
          @po.each do |i|
            if(i[0]=='a')
              i.slice!(0)
              @del=Checkdb.where(date:i)
              if @del.length!=0
                @del[0].delete
              end
            else
              @ins=Checkdb.where(date:i).pluck(:date)
              if(@ins[0]==i)
                next
              else
                @db= Checkdb.create(date:i,value:true)
              end
            end
          end
        end
      end
      @check = Checkdb.all.pluck(:date)
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end

  def GoCDStatus
    begin
      @posts = Gocdstate.find(:all)
      @jso=JSON.parse(@posts.to_json)
      case request.method_symbol
      when :get

        @trac="0"
      when :post
        @trac = params[:result]
      end
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def GoCDUpdate
    begin
      @ans=Array.new
      @iterate = JSON.parse(Gocddiffer.find(:all).to_json)
      i=0
      pipe= Array.new()
      @iterate.each do |item|
        pipe[i]=item["pipeline_name"]
        i+=1
      end
      pipe.uniq!
      i=0
      pipe.each do |item|
        @ans[i] = RestClient::Request.execute method: :get, url: "http://192.168.1.104:8153/go/api/pipelines/" + item + "/history", user: Toolsdb.find(2).username, password: Toolsdb.find(2).password
        @ans[i] = JSON.parse(@ans[i])
        @ans[i] = @ans[i]["pipelines"]
        i+=1
      end

      case request.method_symbol
      when :get

        @trac="0"
      when :post
        @trac = params[:result]
      end
    rescue
      flash[:alert]="There is some problem in accessing data"
    end
  end

  def tracker
    begin
      @tracker= Tracker.find(:all)
      disp = @tracker.to_json
      @t = JSON.parse(disp)
    end
  rescue
    flash[:alert]="There is some problem in accessing data"

  end
end
