class SettingscController < ApplicationController
  def filter
    begin
      case request.method_symbol
      when :get
      when :post
        if params[:stagnated_days]
          dummy=Toolsdb.find(4)
          dummy.password=params[:stagnated_days]
          dummy.save
        elsif params[:logged_hours]
          dummy=Toolsdb.find(5)
          dummy.password=params[:logt_hours]
          dummy.save
        end
      end
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end

  def Changepass
    begin
      case request.method_symbol
      when :get
      when :post
        if params[:redmine]
          user=Toolsdb.find(1).username
          pwd=Toolsdb.find(1).password
          if (pwd==params[:Changepass]['cupass'] && params[:Changepass]['npass']==params[:Changepass]['cnpass'])
            dummy=Toolsdb.find(id)
            dummy.username=params[:Changepass]['user']
            dummy.password=params[:Changepass]['cnpass']
            dummy.save
            if !dummy.save!
              flash[:alert]="your password or username is incorrect"
            end
          else
            flash[:alert] = "Your Password or Username is Incorrect"

          end

        elsif params[:cdgo]
          user=Toolsdb.find(2).username
          pwd=Toolsdb.find(2).password
          if (pwd==params[:Changepass]['cupass'] && params[:Changepass]['npass']==params[:Changepass]['cnpass'])
            dummy=Toolsdb.find(2)
            dummy.username=params[:Changepass]['user']
            dummy.password=params[:Changepass]['cnpass']
            dummy.save
            if !dummy.save!
              flash[:alert1]="your password or username is incorrect"
            end
          else
            flash[:alert1] = "Your Password or Username is Incorrect"
            redirect_to settingsc_Changepass_path
          end

        elsif params[:bb]
          user=Toolsdb.find(3).username
          pwd=Toolsdb.find(3).password
          if (pwd==params[:Changepass]['cupass'] && params[:Changepass]['npass']==params[:Changepass]['cnpass'])
            dummy=Toolsdb.find(2)
            dummy.username=params[:Changepass]['user']
            dummy.password=params[:Changepass]['cnpass']
            dummy.save
            if !dummy.save!
              flash[:alert2]="your password or username is incorrect"
            end
          else
            flash[:alert2] = "Your Password or Username is Incorrect"
            redirect_to settingsc_Changepass_path
          end
        end
      end
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end


  def mailalert
    case request.method_symbol
    when :get
    when :post
      if params[:adminadd]
        begin
          @add = RedmineUser.where(name:params[:admintext])
          @db = Adminlist.create(Name:@add[0].name,emailid:@add[0].mailid)
          @db.save
        rescue
          flash[:adminad]="You can add only the redmine users"
        end
      elsif params[:admindel]
        begin
          @del=Adminlist.where(Name:params[:admintext])
          @del[0].delete
        rescue
          flash[:adminde]="You can delete only the already added admin users"
        end
      elsif params[:excluadd]
        begin
          @add=RedmineUser.where(name:params[:exclutext])
          @db= Exclulist.create(Name:@add[0].name,emailid:@add[0].mailid)
          @db.save
        rescue

          flash[:userad]="You can add only the redmine users"
        end
      elsif params[:excludel]
        begin
          @del=Exclulist.where(Name:params[:exclutext])
          @del[0].delete
        rescue
          flash[:userde]="You can delete only the already added admin users"
        end
      end
    end
    @admin=Adminlist.all
    @exclu=Exclulist.all
  end

  def retrieve
    @names= RedmineUser.order(:name).where("name like ?","%#{params[:term]}%")
    render json: @names.map(&:name)

  end
end
