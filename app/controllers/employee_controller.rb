class EmployeeController < ApplicationController

  def index
    begin
      if params[:delete]
        @mod=Employee.find(params[:id]).destroy
        redirect_to employee_index_path
      elsif params[:edit]
        @@find=Employee.find(params[:id])
        redirect_to employee_edit_path
      end
      @employees=Employee.all
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end

  def show
    begin
      case request.method_symbol
      when :post
        if !params[:empLeave].nil? && !params[:leavef].nil? && !params[:leavet].nil? && !params[:message].nil?

          if RedmineUser.where(name:params[:empLeave]).length!=0
            @employee = Employee.create(name:params[:empLeave] ,dfrom:params[:leavef] ,dto:params[:leavet],reason: params[:message])
            redirect_to employee_index_path
          else
            flash[:alert]="You can add only the redmine users"
          end
        end
      when :get
        Employee.where(dfrom:@current_date)
      end
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end

  def edit
    begin
      @empl=@@find
    rescue
      flash[:alert]="You can't edit an empty database"
    end
  end

  def modify
    begin
      if params[:update]
        mod=Employee.find(params[:id])
        mod.reason=params[:message]
        mod.dfrom=params[:leavef]
        mod.dto=params[:leavet]
        mod.save
        redirect_to employee_index_path
      end
    rescue
      flash[:alert]="There is some problem in accessing database"
    end
  end

  def retrieve
    @names= RedmineUser.order(:name).where("name like ?","%#{params[:term]}%")
    render json: @names.map(&:name)

  end
end
