class BitbucController < ApplicationController
  def prstatus
    begin

      Hashie.logger = Logger.new(nil)
      login=Toolsdb.find(3).username
      password=Toolsdb.find(3).password
      bitbucket = BitBucket.new login:login,password:password
      j=0
      owner="dopms"
      slug=Array.new()
      bitbucket.repos.list do |repo|
        if(repo["owner"]=="#{owner}")
          slug[j]=repo.slug
          j+=1
        end
      end
      length=slug.length
      repos=Array.new(length)
      i=0
      while i<length do
        repos[i]=bitbucket.repos.pull_request.all 'dopms',"#{slug[i]}"
        i+=1
      end
      i=0
      h=0
      @hash=Hash.new()
      while i<length do
        j=repos[i]["size"]
        size=0
        while size<j do
          if(repos[i]["values"][size]["state"]=="OPEN")
            @hash[h]={}
            @hash[h]["count"]=h+1
            @hash[h]["id"]=repos[i]["values"][size]["id"]
            updated=Date.parse repos[i]["values"][size]["updated_on"]
            current=Date.parse(Time.now.to_s)
            different=(current-updated).round
            @hash[h]["updated"]="#{different} days ago"
            @hash[h]["description"]=repos[i]["values"][size]["description"]
            @hash[h]["repository"]=repos[i]["values"][size]["destination"]["repository"]["name"]
            @hash[h]["branch"]=repos[i]["values"][size]["source"]["branch"]["name"]
            h+=1;
          end
          size+=1;
        end
        i+=1;

      end

    end
  rescue
    flash[:alert]="There is some problem in accessing data"
  end
end
