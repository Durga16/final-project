class IssueCollection < ActiveResource::Collection
  def initialize(parsed = {})
    @elements = parsed['issues']
  end
end

class Issue < ActiveResource::Base
  self.site = 'http://192.168.1.121/'
  self.user = Toolsdb.find(1).username
  self.password = Toolsdb.find(1).password
  self.collection_parser = IssueCollection

end
