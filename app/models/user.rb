class UserCollection < ActiveResource::Collection
  def initialize(parsed = {})
    @elements = parsed['users']
  end
end

class User < ActiveResource::Base
  #id=1
  self.site='http://192.168.1.121/'
  self.user=Toolsdb.find_by_id(1).username
  self.password=Toolsdb.find_by_id(1).password
  self.collection_parser = UserCollection
end
