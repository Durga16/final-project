Rails.application.routes.draw do
  root 'status#dashboard'
  #root 'employee#index'
  post "settingsc/mailalert"
  post 'settingsc/Changepass'
  get 'settingsc/mailalert'
  get 'settingsc/filter'
  post 'settingsc/filter'
  get 'status/GoCDStatus'
  post 'status/GoCDStatus'
  get 'status/GoCDUpdate'
  post 'status/GoCDUpdate'
  get 'status/organization'
  post 'status/verification'
  get 'status/verification'
  get 'status/waiting'
  post 'status/waiting'
  get 'status/dashboard'
  post 'status/boardfilter'
  get 'status/closed'
  post 'status/closed'
  get 'status/stagnated'
  post 'status/stagnated'
  get 'bitbuc/prstatus'
  get 'employee/index'
  get 'employee/show'
  post 'employee/edit'
  get 'employee/edit'
  get 'employee/retrieve'
  post 'employee/index'
  post 'employee/show'=>'employee/modify'
  post 'employee/modify'
  get 'employee/modify'
  post 'employee/delete'
  get 'settingsc/Changepass'
  get 'settingsc/retrieve'
  delete 'employee/show'
  match '/', to: 'status#dashboard', via: [:get, :post]
  #get 'passwords/edit'
  #devise_for :devop_users, controllers: {
  # sessions: 'devopuser/sessions'}

  get 'status/organization' => 'status/organization_view'
  post 'status/organization' => 'status/organization_edit'
  Rails.application.routes.draw do
    devise_for :devop_users, controllers: {
      sessions: 'devopuser/sessions'
    }
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
