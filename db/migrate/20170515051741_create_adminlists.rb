class CreateAdminlists < ActiveRecord::Migration
  def change
    create_table :adminlists do |t|
      t.primary_key :Name,:string
      t.string :emailid
      t.timestamps null: false
    end
  end
end
