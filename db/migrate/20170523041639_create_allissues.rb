class CreateAllissues < ActiveRecord::Migration
  def change
    create_table :allissues do |t|
      t.integer :Issue_id
      t.date :Date
      t.integer :Status_Id
      t.string :Status
      t.integer :Assignee_Id
      t.string :Assignee_Name
      t.timestamps null: false
    end
  end
end
