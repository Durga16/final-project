class CreateToolsdbs < ActiveRecord::Migration
  def change
    create_table :toolsdbs do |t|
      t.string :username
      t.string :password
      t.timestamps null: false
    end
  end
end
