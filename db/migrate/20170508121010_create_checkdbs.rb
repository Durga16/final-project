class CreateCheckdbs < ActiveRecord::Migration
 def change
   create_table :checkdbs do  |t|
     t.string :date
     t.boolean :value
     t.timestamps null: false
   end
 end
end
