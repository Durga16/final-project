class CreateChangedissues < ActiveRecord::Migration
  def change
    create_table :changedissues do |t|
      t.integer :Issue_id
      t.integer :Old_Status_Id
      t.string :Old_Status_Name
      t.integer :New_Status_Id
      t.string :New_Status_Name
      t.string :Assignee_Name
      t.date :Updated_Time
      t.timestamps null: false
    end
  end
end
