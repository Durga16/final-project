# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170523041647) do

  create_table "adminlists", force: :cascade do |t|
    t.string   "Name"
    t.string   "emailid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "allissues", force: :cascade do |t|
    t.integer  "Issue_id"
    t.date     "Date"
    t.integer  "Status_Id"
    t.string   "Status"
    t.integer  "Assignee_Id"
    t.string   "Assignee_Name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "changedissues", force: :cascade do |t|
    t.integer  "Issue_id"
    t.integer  "Old_Status_Id"
    t.string   "Old_Status_Name"
    t.integer  "New_Status_Id"
    t.string   "New_Status_Name"
    t.string   "Assignee_Name"
    t.date     "Updated_Time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "checkdbs", force: :cascade do |t|
    t.string   "date"
    t.boolean  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "devop_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "devop_users", ["email"], name: "index_devop_users_on_email", unique: true
  add_index "devop_users", ["reset_password_token"], name: "index_devop_users_on_reset_password_token", unique: true

  create_table "employees", force: :cascade do |t|
    t.string   "name"
    t.string   "dfrom"
    t.string   "dto"
    t.text     "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exclulists", force: :cascade do |t|
    t.string   "Name"
    t.string   "emailid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "redmine_users", id: false, force: :cascade do |t|
    t.string   "name"
    t.string   "mailid"
    t.string   "firstname"
    t.string   "lastname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "toolsdbs", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
