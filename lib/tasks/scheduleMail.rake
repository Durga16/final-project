namespace :scheduleMail do
  desc "Sends notifications"
  task :mailsend => :environment do
    @current_date=Date.today.to_s

    if Checkdb.where(date:@current_date).length==0
      @user=User.find(:all)
      @disp = @user.to_json
      @dis=JSON.parse(@disp)
      @unlogged_A=Array.new(@dis.length){Array.new(4)}
      @list_of_holiday_users=Array.new(@dis.length){Array.new(2)}
      @exclusion_lists=Array.new(@dis.length){Array.new(2)}
      j=0
      i=0
      @dis.each do |ss|
        @timeentries=TimeEntries.find(:all, :params => { :spent_on => @current_date, :user_id =>ss['id']})

        @timeentriesjson =  @timeentries.to_json
        @timeentriesjsons = JSON.parse(@timeentriesjson)
        @lent=@timeentriesjsons.length
        if @lent==0
          @unlogged_A[j][0]=ss['login']
          @unlogged_A[j][1]=ss['mail']
          @unlogged_A[j][2]=ss['firstname']
          @unlogged_A[j][3]=ss['lastname']
          j+=1
        end
      end

      @exclude=Exclulist.all
      current_date=Date.today.to_s
      current_date= current_date.tr('-', '')
      current_date = Date.parse current_date
      @unlogged_A.each do |ua|
        if Exclulist.where(Name:ua[0]).length==1
          ua.pop
          ua=nil
        else

          @employees = Employee.where(name:ua[0])

          if @employees.length==0
            next
          else
            @employees.each do |empl|
              from_date=Date.strptime(empl['dfrom'],"%m/%d/%Y")
              to_date=Date.strptime(empl['dto'],"%m/%d/%Y")
              current_date.between?(from_date,to_date)

              if current_date.between?(from_date,to_date)
                @list_of_holiday_users[i][0]=ua[2]
                @list_of_holiday_users[i][1]=ua[3]
                i+=1
                ua.pop
                ua=nil
              end
            end
          end
        end
      end
      @unlogged_A.each do |ua|
        if ua[2]!=nil && ua[3]!=nil
          Unlogged.sendUnloggedmail(ua[1],ua[2],ua[3]).deliver_now
        end
      end
      @dis.each do |ss|
        @timeentries=TimeEntries.find(:all, :params => { :spent_on => @current_date, :user_id =>ss['id']})
        @timeentriesjson =  @timeentries.to_json
        @timeentriesjsons = JSON.parse(@timeentriesjson)
        @lent=@timeentriesjsons.length
        i=0
        if @lent!=0
          @total_time_spent=0
          @timeentriesjsons.each do
            @total_time_spent+=@timeentriesjsons[i]['hours']
            i+=1
          end
        end
      end
      @list_of_holiday_users.each do |le|
        if le[0]!=nil && le[1]!=nil
        end
      end
      @admins= Adminlist.all
      @admins.each do |admin|
        Sendmail.mailing(admin['emailid'],@dis,@list_of_holiday_users,@unlogged_A).deliver_now
      end
    end
  end
end
