namespace :updatedb do
  task :dashboard => :environment do
    @stagnated= Issue.find(:all, :params => { :limit => 100 })
    disp = @stagnated.to_json
    d3 = JSON.parse(disp)
    d3.each do |itm|
      @iss=Allissue.where(Issue_id:itm["id"])
      if(@iss.length==0)
        Allissue.create(Issue_id:itm["id"],Date:itm["updated_on"],Status_Id:itm["status"]["id"],Status:itm["status"]["name"],Assignee_Id:itm["assigned_to"]["id"],Assignee_Name:itm["assigned_to"]["name"])
      elsif (@iss[0].Status_Id != itm["status"]["id"])
        Changedissue.create(Issue_id:itm["id"],Old_Status_Id:@iss[0].Status_Id,Old_Status_Name:@iss[0].Status, New_Status_Id:itm["status"]["id"], New_Status_Name:itm["status"]["name"],Assignee_Name:itm["assigned_to"]["name"], Updated_Time:itm["updated_on"])
        @iss[0].Status_Id = itm["status"]["id"]
        @iss[0].Status = itm["status"]["name"]
        @iss[0].Date = itm["updated_on"]
        @iss[0].save
      end
    end
  end
end
